"""
sam_alert_rules_api provides an api capable of managing prometheus rules, to manage alerts. Also
it allows to fetch all user rules available

* Copyright (C) 2022-2023 Atlar Innovation - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
"""


import os

import requests
import yaml
from fastapi import APIRouter

from ska_alerts_api.ska_alerts_model import Rule

api = APIRouter()
alerts = APIRouter()

api.include_router(alerts, prefix="/alerts")

prom_address = os.environ.get(
    "PROMETHEUS_ADDRESS",
    "http://localhost:9090",
)

api_rules_prom_path = os.environ.get(
    "API_RULES_PROM_PATH",
    "/tmp/rules/",
)


def into_yml_file(json_var, file_path):
    """
    Transforms json into yml
    and writes it into yml file

    :param json_var: Json var to be transformed to yml
    :param file_path: Path of the yml file
    :return: 200
    """
    # Convert the dictionary to YAML format
    yaml_data = yaml.dump(json_var)

    # Write the YAML data to a file
    with open(file_path, "w", encoding="utf-8") as file:
        file.write(yaml_data)


@api.post("/create")
async def create_alerting_rule(request: Rule):
    """
    Creates alert rule and reloads prometheus
    configuration

    :param Rule: Value of Rule to be created
    :return: 200
    """

    file_path = api_rules_prom_path + request.user + ".rules"

    rule = {
        "alert": request.name,
        "expr": request.expr,
        "for": request.time,
        "labels": {
            "severity": request.severity,
        },
        "annotations": {"summary": f"This rule was created by {request.user}", "descriprion": "Test"},
    }

    if os.path.exists(file_path):
        # Retrives data and appends new rule
        with open(file_path, "r", encoding="utf-8") as file:
            yaml_data = file.read()
        group_rules = yaml.load(yaml_data, Loader=yaml.FullLoader)
        if rule not in group_rules["groups"][0]["rules"]:
            group_rules["groups"][0]["rules"].append(rule)
        else:
            return "shit"
    else:
        group_rules = {"groups": [{"name": f"{request.user}", "rules": [rule]}]}

    into_yml_file(group_rules, file_path)
    # Reload prometheus config
    requests.post(f"{prom_address}/-/reload", timeout=1)
    return 200


@api.post("/delete")
async def delete_alerting_rule(request: Rule):
    """
    Deletes alert rule and reloads prometheus
    configuration

    :param Rule: Value of Rule to be created
    :return: 200
    """

    file_path = api_rules_prom_path + request.user + ".rules"

    rule = {
        "alert": request.name,
        "expr": request.expr,
        "for": request.time,
        "labels": {
            "severity": request.severity,
        },
        "annotations": {"summary": f"This rule was created by {request.user}", "descriprion": "Test"},
    }

    if os.path.exists(file_path):
        # Retrives data and appends new rule
        with open(file_path, "r", encoding="utf-8") as file:
            yaml_data = file.read()
        group_rules = yaml.load(yaml_data, Loader=yaml.FullLoader)
        if rule in group_rules["groups"][0]["rules"]:
            group_rules["groups"][0]["rules"].remove(rule)
            into_yml_file(group_rules, file_path)
            # Reload prometheus config
            requests.post(f"{prom_address}/-/reload", timeout=1)
        else:
            return "shit"

    return 200


@api.get("/get_rules")
def get_prometheus_api_response(user: str):
    """
    Get alert rules from specific user

    :param user: name of the user that is
    used as group name when creating a rule
    :return: 200
    """

    url = f"{prom_address}/api/v1/rules"
    response = requests.get(url, timeout=1)
    response = response.json()
    device_rules = []
    for group in response["data"]["groups"]:
        if group["name"] == user:
            device_rules = group["rules"]
            break

    return device_rules  # Return the JSON response