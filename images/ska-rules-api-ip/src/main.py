#!/usr/bin/env python

import os

import uvicorn
from fastapi import APIRouter, FastAPI
from prometheus_fastapi_instrumentator import Instrumentator

from ska_alerts_api.ska_alerts_rules_api import api as rules_api


VERSION = "v0.0.1"

# FastAPI and Router objects
app = FastAPI(
    title="SKA Alert Manager",
    version=VERSION,
)

api = APIRouter()


app.include_router(rules_api, prefix="/rules")


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=os.environ.get("SKA_REST_API_HOST", "0.0.0.0"),
        port=int(os.environ.get("SKA_REST_API_PORT", 8080)),
        reload=False,
    )
