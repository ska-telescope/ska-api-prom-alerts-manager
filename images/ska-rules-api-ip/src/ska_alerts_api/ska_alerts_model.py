
from pydantic import BaseModel

class Rule(BaseModel):
    """
    Prometheus Rule used to trigger alerts
    on prometheus that alertmanager uses
    to report

    * user: User creating the rule
    * name: Name of the rule
    * expr: Expression used for the rule
    * severity: Severity of the rule
    * time: Time for verification
    """

    user: str
    name: str
    expr: str
    severity: str
    time: str