from prometheus_client import start_http_server, Gauge
import random
import time

# Define a Prometheus gauge to track the value of the variable
variable_gauge = Gauge('my_variable', 'Description of my variable')

# Start the Prometheus HTTP server on port 8000
start_http_server(8000)

# Generate a random value for the variable every second and update the gauge
while True:
    variable_value = random.randint(0, 100)
    variable_gauge.set(variable_value)
    time.sleep(1)