include .make/base.mk
include .make/python.mk
include .make/oci.mk

PYTHON_MAX_LINE_LENGTH = 120
PYTHON_SWITCHES_FOR_BLACK = --line-length $(PYTHON_MAX_LINE_LENGTH)
PYTHON_SWITCHES_FOR_ISORT = -w $(PYTHON_MAX_LINE_LENGTH)
PYTHON_SWITCHES_FOR_FLAKE8 = --max-line-length $(PYTHON_MAX_LINE_LENGTH)
PYLINT_IGNORES = "fixme"
PYLINT_PKG_WHITELIST = pydantic
PYTHON_SWITCHES_FOR_PYLINT = --max-line-length $(PYTHON_MAX_LINE_LENGTH) \
	--disable $(PYLINT_IGNORES) \
	--extension-pkg-whitelist $(PYLINT_PKG_WHITELIST) \
	--min-public-methods 0 \
	--max-args 10 \
	--max-attributes 16 \
	--max-returns 10 \
	--max-parents 10 \
	--max-locals 50 \
	--max-nested 10
PYTHON_LINT_TARGET = src/

local_alertmanager:
	docker run -p 9093:9093 ska-alertmanager-image:0.0.0

local_prometheus:
	docker run --network host -p 9090:9090 -v /tmp/rules:/etc/prometheus/user_rules/ artefact.skao.int/ska-prometheus-image:0.0.0 --web.enable-lifecycle --config.file=/etc/prometheus/prometheus.yml

local_api:
	docker run --network host -p 8080:8080 -v /tmp/rules:/tmp/rules/ artefact.skao.int/ska-rules-api-ip:0.0.0

local_exporter:
	docker run -it  -p 8000:8000 artefact.skao.int/ska-metrics-expoter-ip:0.0.0